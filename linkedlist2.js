//linkedlist
  //Head
  //Node - holds the Data
  //Pointer
  //Tail


//create a Node Class with data, next=null as parameter for constructor
  class Node{
    constructor(data, next=null){
      this.data = data;
      this.next = next;
    }
  }

  //create a Linked List class
  class LinkedList{
    constructor(){
      this.head = null;
      this.size = 0;
    }

    //To insert in the first Node
    insertFirst(data){
      //the new data will push the head node as the next data
      //and the new data will become the head
      this.head = new Node(data, this.head);
      //increase the size of the linked list
      this.size++;
    }

    //To insert at the last Node
    insertLast(data){
      //create a new node
      let node = new Node(data);
      //declare a variable current
      let current;
      // check the head if empty, store the data in head node
      if(this.head === null){
        this.head = node;
      }else{
        current = this.head;
        while(current.next !== null){
          current = current.next;
        }
        current.next = node;
      }
      //increment the size
      this.size++;
    }

    //To insert at an index
    insertAt(data, index){
      //if index is out of range
      if((index < 0) || (index > this.size)){
        return;
      }
      //if it's the first index
      if(index === 0){
        this.insertFirst(data);
        return;
      }
      //if not firts and valid index
      const node = new Node(data);
      let current, previous;

      //Set current to first
      current = this.head;
      let count = 0;

      while(count < index){
        previous = current; //Node before the index
        count++;
        current = current.next; //Node after the index
      }
      node.next = current;
      previous.next = node;
      this.size++;
    }

    //To get at index
    getAt(index){
      //if index is invalid
      if((index < 0) || (index > this.size)){
        return;
      }

      let count = 0;
      let current = this.head;
      while(current !== null){
        if(count === index){
          console.log(current.data);
        }
        count++;
        current = current.next;
      }
      return null;
    }

    //To remove at index
    removeAt(index){
      //if index is invalid
      if((index < 0) || (index > this.size)){
        return;
      }

      //declare and intialize the ff variables
      let current = this.head;
      let previous
      let count = 0;

      //if index is the first
      if(index === 0){
        this.head = current.next;
      }else{
        while(count < index){
          count++;
          previous = current;
          current = current.next;
        }
        previous.next = current.next;
      }
      this.size--;
    }

    //To clear the list
    clearList(){
      this.head = null;
      this.size = 0;
    }

    //To print list data
    printListData(){
      //declare a variable current and initialize it with this.head
      let current = this.head;
      //loop through all the nodes using while loop
      while(current !== null){
        console.log(current.data);
        //traverse another node
        current = current.next;
      }
    }
  }

  const ll = new LinkedList();
  ll.insertFirst(100);
  ll.insertFirst(200);
  ll.insertFirst(300);
  ll.insertLast(400);
  ll.insertAt(500, 2);

  //ll.removeAt(2);

  //ll.clearList();

  ll.printListData();
