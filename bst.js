//Binary Search Trees - comprised of data points called nodes
	//the first node (the top of the tree), is called the root
	//a node can only have two branches (1 parent = max of 2 children)
	//nodes with no children are called leaves
	//each LEFT subtree is LESS THAN the parent node
	//each RIGHT subtree is GREATER THAN the parent node
	//due to its binary nature, operations will be able to skip half of the tree
	//making lookups, insertions, and deletions faster than those performed on arrays

let Node = function(data){
	this.data = data;
	this.left = null;
	this.right = null;
}

let BinarySearchTree = function(){
	this.root = null;

	this.add = function(data){
		//declare a node constant that will serve as our reference to the root node
		const node = this.root;
		if(node === null){
			this.root = new Node(data);
			return;
		}else{
			//create a function expression that will search where the passed in data can be inserted
			//in the tree
			const searchTree = function(node){
				//if data to be added is less that the current root
				if(data < node.data){
					if(node.left === null){
						//point the left pointer of current node to the newly instantiated node
						node.left = new Node(data);
						return;
					}else{ //if there's a left subtree
						//continue searching the tree recursively
						return searchTree(node.left);
					}
				//if data to be added is greater than the current root
				}else if(data > node.data){
					if(node.right === null){
						node.right = new Node(data);
						return;
					}else{
						return searchTree(node.right);
					}
				}else{
					//duplicate data will fall here, we don't want to insert duplicate data in our BST
					return null;
				}		
			}
			//invoke the previously defined function expression searchTree() using the root node as the initial
			//argument
			return searchTree(node);
		}
	}

	//define a method findMin() that will find the node with the minimum value in a BST
	this.findMin = function(){ 
	    // if left of a node is null 
	    // then it must be minimum node 
	    let node = this.root;
	    while(node.left){
	    	node = node.left;
	    }
	    return node.data;
	} 

	//define a method findMax() that will find the node with the maximum value in a BST
	this.findMax = function(){ 
	    // if right of a node is null 
	    // then it must be maximum node 
	    let node = this.root;
	    while(node.right){
	    	node = node.right;
	    }
	    return node.data;
	} 
	//define a method isPresent() that will check whether of not a passed in value is present in the BST
	//this will return a boolean value
	this.isPresent = function(data){
		const node = this.root;
		if(node === null){
			return null;
		}else{
			const searchTree = function(node){
				// if data is less than node data move left
				if(data === node.data){
					return true;
				}else if(data < node.data){
					if(node.left === null){
						return false;
					}else{
						return searchTree(node.left);	
					}
				}else if(data > node.data){
					if(node.right === null){
						return false;
					}else{
						return searchTree(node.right);
					}
				}
			}
			return searchTree(node);
		}
	//define a function inOrder();
	this.inOrder = function(){
		let node = this.root;
		if(node === null){
			return null;
		}else{
			//function to get the min
			const getMins = function(node){
				//get to the left most node
				while(node.left){
					node = node.left;
				}
				let minimum = node.data;
				console.log(minimum);

				//function to check right sub tree
				const checkRight = function(node){
					if(node.right === null){
						return;
					}else{
						return getMins(node.right);
					}
				}
				return checkRight(minimum);
			}
		}
		
	}
	


	}
}

//find the min
//find the max
//find a node