//Recursion is when a function calls itself
//it has three components

	//termination condition - like an emergency break, it acts as your fail-safe
	//it will prevent the recursion from running under bad circumstances(ie bad input received)

	//base case - the objective or the goal of your recursion, when this is met your recursion can stop
	//the actual recursion wherein the function will call itself


//define a function that can take in a whole number and return its factorial
function factorial(number){
	//TERMINATION - if the given condition is met, exit immediately
	if(number<0) return;
	//BASE CASE - define the objective where the recursion can stop
	if(number === 0) return 1;
	//RECURSION
	return number * factorial(number-1);
}


//detrmine if a given string is a palindrome (same word even if spelled reverse)
//WITHOUT resorting to the reverse() string method

//compare the first character of the string with the last character
	//if !==, return FALSE
	//if ==, proceed inwards while applying the same logic
		//if you run out of characters to compare (either 0 characters left for even numbered
		//or 1 character left for odd numbered) return TRUE

	function palindrome(word){
		//termination - if the word is not a string
		if(typeof word !== "string"){
			return false;
		}

		//base case - once the length is <= 1 stop
		if(word.length<=1){
			return true;
		}

		//recursion
		//compare the 1st and last letter of the word
		if(word[0] === word[word.length-1]){
			word = word.substring(1,word.length-1);
			return palindrome(word);
		}else{
			return false;
		}
	}

//declare an empty array to store prime numbers
let prime = [];

function isPrime(value){
    for(let num = 2; num < value; num++){
        if(value % num === 0){
					console.log(`${value} modulo ${num} is ${value%num} therefore ${value} is not a prime.`);
          return false;
        }
    }
    if(value>1){
			console.log(`${value} is a prime number.`);
    	return true;
    }
}

function primeNumbers(number){
	for(let num = 2; num < number; num++){
		if(isPrime(num)){
			prime.push(num);
			console.log(prime);
		}
	}
}
