//---------------------Stack Data Structure------------------//
//first in, last out
//example: undo action

//methods of a stack = push, pop, peek, length

//define a stack class
let Stack = function(){
	//the stack object has a count property, initially set to zero
	this.count = 0;

	//it also has a storage property which is an empty object
	this.storage = {};

	//it has a push() method that adds a passed in value to the end of the stack's storage
	this.push = function(value){
		//use the count property as an index
		this.storage[this.count] = value;
		//increment the count to reflect the added element
		this.count++;
	}

	//shows the value at the top of the stack
	this.peek = function(){
		return this.storage[this.count-1];
	}

	//removes the last element === the top of the stack and returns what was removed
	this.pop = function(){
		//if the stack is empty, return undefined, effectively terminating the method earlier (short-circuiting)
		if(this.count === 0){
			return undefined;
		}else{
			//decrement the count
			this.count--;
			//create a variable to temporarily store the element to be deleted
			let result = this.storage[this.count];
			//delete the element at the top of the stack
			delete this.storage[this.count];
			//return deleted value
			return result;
		}
	}

	this.length = function(){
		return this.count;
	}
}

//instantiate a new object from the Stack() class
let myStack = new Stack();
myStack.push(1);
console.log(myStack.peek());
myStack.push(3);
console.log(myStack.peek());
console.log(myStack.length());
console.log(myStack.pop());
console.log(myStack.peek());

//---------------------Queue Data Structure------------------//
//first in, first out
//define a class constructor for a Queue class
//has a single property which is a collection of elements (can be array)
//implement the following methods:
	
	
	
	
	
	
//demonstrate all the methods by instantiating a new object from the Queue class and calling its various methods


let Queue = function(){

	//declare a property array
	this.storage = [];

	//size() - returns the length of the queue
	this.size = function(){
		return this.storage.length;
	}

	//print() - prints the value of the collection
	this.print = function(){
		return this.storage;
	};
	//enqueue() - adds an element to the end of the queue
	this.enqueue = function(num){
		this.storage.push(num);
	}

	//dequeue() - removes the FIRST element from the queue
	this.dequeue = function(){
		return this.storage.shift();
	}

	//front() - returns the first element of the queue
	this.front = function(){
		return this.storage[0];
	}

	//isEmpty() - returns a boolean value on whether or not the queue is empty
	this.isEmpty = function(){
		if(this.size === 0){
			return false;
		}else{
			return true;
		}
	}

}

let myQueue = new Queue();
myQueue.enqueue(2);
myQueue.enqueue(3);
console.log(myQueue.print());
console.log(myQueue.front());
console.log(myQueue.print());
myQueue.dequeue();
console.log(myQueue.print());
console.log(myQueue.isEmpty());


