//define a Node class that will accept in value during instantiation
let Node = function(value){
		//a node has a value equal to the passed in value when it was instantiated
		this.value = value;
		//a node object will have a next property which is a pointer.
		//it initially points to null
		this.next = null;
	}


//linked list are data structures that are made up of nodes each of which has two properties:
	//value
	//next - reference pointer pointing to the next node (if there's any)

//linked list have a head which is just a pointer, it is NOT a node. It points to the first node (if there's any)

//the last node in a linked list will have its next pointer pointing to null

//there's no indexing in a linked list. You have to traverse the list from the head, in order, until you get
//to the node you're looking for

//if a node loses reference (no pointer is pointing at it), that node is removed from the linked list.

//define a LinkedLIst class constructor
let LinkedList = function(){
	//has a head property that initially points to null
	this.head = null;

	//has a length property with an initial value of 0
	this.length = 0;
	
	//returns the length of our LinkedList class
	this.size = function(){
		return this.length;
	}

	//this method returns the first node in a linked list
	this.getHead = function(){
		return this.head;
	}

	//adds a node at the end of the linked list
	//the passed in value will be the value of the node
	this.push = function(value){
		//instantiate a new Node object by passing in the given value to the Node() class constructor
		let node = new Node(value);

		//check if the linked list is empty
		if(this.head === null){
			//add the newly instantiated node
			this.head = node;
		}else{
			//declare a variable that will point to the current node to help us in traversing the linked list
			//to the last node
			let currentNode = this.head;
			//while there's a next node. traverse
			while(currentNode.next){
				//this is the actual traversal of nodes
				currentNode = currentNode.next;
			}
			//currentNode is now the last node
			//point the next pointer of currentNode to the new node
			currentNode.next = node;
		}
		//increment the length after adding the new node to the end of the linked list
		this.length++;
		//return the node that was added to the linked list
		return node;
	}

	//define an unshift() method that will add a node to the start of the linked list
	this.unshift = function(value){
		let node = new Node(value);
		if(this.head === null){
			this.head = node;
		}else{
			//point the next property of the new node to the former first node
			node.next = this.head;
			//point the head of the linked list to the new node
			this.head = node;
		}
		this.length++;
	}

	//define a shift() method that will remove the first node from a linked list
	this.shift = function(){
		if(this.head === null){
			return undefined;
		}else{
			//point the head pointer of the linked list to the next node
			//effectively removing the first node
			this.head = this.head.next;
			//decrement the length of the linked list
			this.length--;
		}
	}


	//removes a node with a given value from the  linked list
	//this.remove = function(value){}
}


//stack = first in, last out type of data structure
	//push() - add to top of stack
	//pop() - remove from the top of stack
	//peek() - return the top of the stack
	//size() - returns the length of the stack
	//isEmpty() - returns a boolean value


//queue = firts in, first out type of data structure
	//enqueue() - add to end of queue
	//dequeue() - remove from the start of the queue
	//front() - returns the first node / element
	//size() - returns the length of the queue
	//isEmpty() - returns a boolean value


//define class constructors for a stack and a queue data structure using Linked list

let Stack = function(){
	//has a head property that initially points to null
	this.head = null;

	//has a length property with an initial value of 0
	this.length = 0;

	this.push = function(value){
		//instantiate a new Node object by passing in the given value to the Node() class constructor
		let node = new Node(value);

		//check if the linked list is empty
		if(this.head === null){
			//add the newly instantiated node
			this.head = node;
		}else{
			//declare a variable that will point to the current node to help us in traversing the linked list
			//to the last node
			let currentNode = this.head;
			//while there's a next node. traverse
			while(currentNode.next){
				//this is the actual traversal of nodes
				currentNode = currentNode.next;
			}
			//currentNode is now the last node
			//point the next pointer of currentNode to the new node
			currentNode.next = node;
		}
		//increment the length after adding the new node to the end of the linked list
		this.length++;
		//return the node that was added to the linked list
		return node;
	}
		

	this.pop = function(){
		let currentNode, previousNode;
		//traverse the end of the linked list
		//check first if the linked list has elements in it
		if(this.head === null){
			return undefined;
		}else{
			//initialize a variable currentNode and previousNodes
			currentNode = this.head;
			previousNode = currentNode;
			//do a while loop to get to the end
			while(currentNode.next){
				previousNode = currentNode;
				currentNode = currentNode.next;
			}
			//when we get at the end of the linked list, initialize the variable previous
			previousNode.next = null;
			//decrement the length
			this.length--;
			return currentNode;
		}
		

	}

	this.peek = function(){ //returns the top of the stack
		let currentNode;
		//let previousNode;
		//check if the stack has elements in it
		if(this.head === null){
			return undefined;
		}else{
			currentNode = this.head;
			while(currentNode.next){
				//previousNode = currentNode;
				currentNode = currentNode.next;
			}
			return currentNode;
		}

	}

	this.size = function(){ //returns the length of the stack
		return this.length;
	}

	this.isEmpty = function(){
		if(this.head === null){
			return true;
		}else{
			return false;
		}
	}
}




let Queue = function(){
	//queue has a head property
	this.head = null;

	//queue has a length property
	this.length = 0;

	this.enqueue = function(value){ //add to end of queue
		//instantiate a new node
		let node = new Node(value);

		//check if the queue has elements in it
		if(this.head === null){
			this.head = node;
		}else{
			let currentNode = this.head;
			while(currentNode.next){
				currentNode = currentNode.next;
			}
			currentNode.next = node;
		}
		this.length++;
		return node;
	}

	this.dequeue = function(){ //remove from the start of the queue
		//check if the queue has elements in it
		if(this.head === null){
			return undefined;
		}else{
			this.head = this.head.next;
			this.length--;
		}

	}

	this.front = function(){ //returns the first node / element
		if(this.head === null){
			return undefined;
		}else{
			return this.head;	
		}
	}

	this.size = function(){
		return this.length;	
	}

	this.isEmpty = function(){
		if(this.head === null){
			return true;
		}else{
			return false;
		}
	}
}