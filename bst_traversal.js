const Node = function(value){
	this.value = value;
	this.left = undefined;
	this.right = undefined;

	//implement the logic of creating a BST in the add() method of the Node class
	this.add = function(value){
		if(value < this.value){
			if(this.left === undefined){
				this.left = new Node(value);
				return this; //returns the parent node where the new node is inserted
			}else{ // a left subtree exists
				//recrusively call this node's add method from the left pointer
				return this.left.add(value);
			}
		}else if(value > this.value){
			if(this.right === undefined){
				this.right = new Node(value);
				return this;
			}else{
				return this.right.add(value);
			}
		}else{
			return "No duplicates allowed.";
		}
	}

	//traverse the BST in an asecending manner with respect to node values
	this.inOrder = function(){
		if(this.left){
			this.left.inOrder();
		}
		console.log(this.value);
		if(this.right){
			this.right.inOrder;
		}
	}

	//returns the order of node insertion that will duplicate the BST
	this.preOrder = function(){
		console.log(this.value);
		if(this.left){
			this.left.preOrder();
		}
		if(this.right){
			this.right.preOrder;
		}
	}

	//returns the order of node deletion that will prevent tree restructuring
	this.postOrder = function(){
		if(this.left){
			this.left.postOrder();
		}
		if(this.right){
			this.right.postOrder();
		}
		console.log(this.value);
	}
}

let root = new Node(8); //this will be the root of the BST since this is the first node to be instantiated
root.add(9); //succeeding nodes that will be added to our first node will be added in a manner that will form a BST
root.add(23);